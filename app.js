const Discord = require('discord.js');
const client = new Discord.Client();
const fs = require('fs')


const data = JSON.parse(fs.readFileSync('./key.txt', 'utf8'));
const token = data['1'];
const prefix = '!'
const active = new Map();
let listActivities = JSON.parse(fs.readFileSync('./listactivities.json', 'utf8')).list

client.on('message', message => {
	let args = message.content.slice(prefix.length).trim().split(' ');
	let cmd = args.shift().toLowerCase();

	if (message.author.bot) return;
	if (!message.content.startsWith(prefix)) return;

	try{

		let ops = {
			active: active
		}
		delete require.cache[require.resolve(`./commands/${cmd}.js`)];

		let commandFile = require(`./commands/${cmd}.js`)
		
		commandFile.run(client, message, args, ops);
	} catch (e) {
		console.log(e.stack);
	}

});


client.on('ready', async() => {
	let cur = Math.round(Math.random() * listActivities.length-1)
	let tmp = listActivities[cur]
	while(!tmp) tmp = listActivities[cur-1]
	client.user.setActivity(listActivities[cur].text, listActivities[cur].data)
	delete tmp

	setInterval( () => {
		let cur = Math.round(Math.random() * listActivities.length-1)
		let tmp = listActivities[cur]
		while(!tmp) tmp = listActivities[cur-1]
		client.user.setActivity(listActivities[cur].text, listActivities[cur].data)
	}, 60000)

	console.log('ready'); 
})

client.login(token)