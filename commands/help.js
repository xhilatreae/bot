const fs = require('fs')

exports.run = (client, message, args, ops) => {
	let resp = '**commands are: **\n';

	fs.readdir(__dirname, (err, files) => {
		files.forEach(file => {
			resp += `__`+file.slice(0, file.length-3) + `__`+ '\n'
		})
			message.channel.send(resp);
	})
}