exports.run = (client, message, args, ops) => {
	let fetched = ops.active.get(message.guild.id)

	if(!fetched) return message.channel.send('music is not playing')

	if(fetched.dispatcher.paused) {
		fetched.dispatcher.resume();
	} else {
		fetched.dispatcher.pause();
	}
}