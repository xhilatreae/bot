const ytdl = require('ytdl-core');
const search = require('yt-search');
const { DataResolver, MessageEmbed } = require('discord.js');

exports.run = async (client, message, args, ops) => {
	start(client, message, args, ops)
}

async function start(client, message, args, ops){
	if(!message.member.voice.channel) return message.channel.send('not in voice');

	if(!args[0]) return message.channel.send('not a url');

	let validate = await ytdl.validateURL(args[0]);

	if(!validate) {
		args = args.join(' ')
		YTSearch(client, message, args, ops);
	} else { 
		let info = await ytdl.getInfo(args[0]);

		let data = ops.active.get(message.guild.id) || {};
		if(!data.connection) data.connection = await message.member.voice.channel.join();
		if(!data.queue) data.queue = [];
		data.guildID = message.guild.id; 

		data.queue.push({
			songTitle: info.title,
			url: args[0],
			announceChannel: message.channel.id
		});
		
		if(!data.dispatcher) {
			await play(client, ops, data)
			//message.reply('prikol')
		}
		else {
			//message.channel.send(`added to queue: ${info.title}`)
		}

		ops.active.set(message.guild.id, data);
		let embed = await createEmbed(info.videoDetails.videoId, message.author.username)
		await message.channel.send(embed)
		message.delete({timeout: 250})

		
	}
}


async function play(client, ops, data) {
	//client.channels.get(data.queue[0].announceChannel).send(`playing: ${data.queue[0].songTitle}`)
	return new Promise( async resolve => {
		data.dispatcher = await data.connection.play(ytdl(data.queue[0].url, {filter: 'audioonly'}))
		data.dispatcher.guildID = data.guildID
		data.dispatcher.once('skip', function() {
			finish(client, ops, this);
		})
		data.dispatcher.on('finish', function() {
			finish(client, ops, this)
		})

		resolve(true)
	})
	
}

function finish(client, ops, dispatcher) {
	let fetched = ops.active.get(dispatcher.guildID)
	if(fetched.queue.length > 1) {
		fetched.queue.shift();
		ops.active.set(dispatcher.guildID, fetched)
		play(client, ops, fetched)
	} else {
		ops.active.delete(dispatcher.guildID);
		let vc = client.guilds.cache.get(dispatcher.guildID).me.voice.channel
		if (vc) vc.leave();
	}
}

YTSearch = async(client, message, args, ops) => {
	search(args, async(err, res) => {
		if(err)  {
			return message.channel.send('something went wrong')
		}
		let videos = res.videos.slice(0, 10); 
		let resp = ''
		videos.forEach((video, index) => {
			let title = video.title.substring(0, 80)
			resp += `**[${parseInt(index)+1}]:** \`${title}\`\n`;
		})
		let msg = await message.channel.send(resp)
			.then(async msg => { return new Promise(resolve => {resolve(msg)}) });
		
		let selected = null
		let q_ = async() =>{
			let i = 0;

			await msg.react('1️⃣'); i++; if (selected != null) return; i++
			await msg.react('2️⃣'); i++; if (selected != null) return; i++
			await msg.react('3️⃣'); i++; if (selected != null) return; i++
			await msg.react('4️⃣'); i++; if (selected != null) return; i++
			await msg.react('5️⃣'); i++; if (selected != null) return; i++
			await msg.react('6️⃣'); i++; if (selected != null) return; i++
			await msg.react('7️⃣'); i++; if (selected != null) return; i++
			await msg.react('8️⃣'); i++; if (selected != null) return; i++
			await msg.react('9️⃣'); i++; if (selected != null) return; i++
			await msg.react('🔟'); i++; if (selected != null) return; i++
			await msg.react('❌'); i++; if (selected != null) return; i++
		}; q_()
		let filter = (reaction, user) => {
			return  ['1️⃣','2️⃣','3️⃣','4️⃣','5️⃣','6️⃣','7️⃣','8️⃣','9️⃣','🔟','❌'].includes(reaction.emoji.name) && !user.bot
		}
		rCollector = msg.createReactionCollector(filter, {time: 60000});
		rCollector.once('collect', async(reaction, user) => {
			rCollector.stop()
			switch (reaction.emoji.name){
				case '1️⃣': selected = 1; break
				case '2️⃣': selected = 2; break
				case '3️⃣': selected = 3; break
				case '4️⃣': selected = 4; break
				case '5️⃣': selected = 5; break
				case '6️⃣': selected = 6; break
				case '7️⃣': selected = 7; break
				case '8️⃣': selected = 8; break
				case '9️⃣': selected = 9; break
				case '🔟': selected = 10; break
				case '❌': selected = 0; break
			}
			if (selected) {
				start(client, message, [videos[selected-1].url], ops)
				msg.delete({timeout: 250})
			} else {
				msg.edit('declined')
				msg.reactions.removeAll()
			}
		})
	});
}


createEmbed = async(param, requester) => {
	return new Promise( resolve => {
		search({videoId: param}, async(e, r) => {
			const embed = new MessageEmbed()
			.setTitle(r.title)
			.setURL(r.url)
			.setAuthor(`${r.author.name} | ${r.duration.toString()}`, '', r.author.url)
			.setThumbnail(r.thumbnail)
			
			if (requester) embed.setFooter(`queued by: ${requester}`)
			resolve(embed)
		})
	})	
}