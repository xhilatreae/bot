exports.run = async (client, message, args, ops) => {

	let fetched = ops.active.get(message.guild.id)
	if(!fetched) return message.channel.send('queue is empty');

	let queue = fetched.queue
	let nowPlaying = queue[0]

	let resp = `**Now playing**\n`+
	`__${nowPlaying.songTitle}__\n\n`


	if(queue[1] != undefined){
		resp += 	`**queue**\n`;
		for (let i = 1; i < queue.length; i++)
			resp += `${i}. __${queue[i].songTitle}__\n`;
	}
	message.channel.send(resp);	
	message.delete();

}