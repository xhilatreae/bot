exports.run = async (client, message, args, ops) => {
	let fetched = ops.active.get(message.guild.id);

	if (!fetched) return message.channel.send('music is not playing')

	if(message.member.voiceChannel !== message.guild.me.voiceChannel) return message.channel.sent('you are not in the same voice channel')

	return fetched.dispatcher.emit('skip')
}