exports.run = (client, message, args, ops) => {
	let fetched = ops.active.get(message.guild.id);
	fetched.queue = [];
	return fetched.dispatcher.emit('skip');
}