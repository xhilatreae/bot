exports.run = (client, message, args, ops) => {
	let fetched = ops.active.get(message.guild.id)

	if(!fetched) return message.channel.send('music is not playing')

	if(message.member.voiceChannel != message.guild.me.voiceChannel) return message.channel.send('sorry, you are not in the same channel')

	if (isNaN(args[0]) || args[0] > 100 || args[0] < 0) return message.channel.send('input number between 0-100');

	fetched.dispatcher.setVolume(args[0]/100)	
}